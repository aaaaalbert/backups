// MAP.js based on leaflet.js
// map.js, map.html and map.css and map.cfg.example 
// v07022017.0000
//
// Copyright (c) 2015-2017, "Erich N. Pekarek" <erich@pekarek.priv.at>
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice, this
//    list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies,
// either expressed or implied, of the FreeBSD Project.
//
function insertJSLib(JSURL){
	var js = document.createElement('script');
	js.src = JSURL;
	js.type = 'text/javascript';
	js.onload = function() { console.log(JSURL + "loaded."); };
	var loc = JSURL.split('://',1);
	if ((loc[0] == 'http') || (loc[0] == 'https')) {
		js.async=1;
		js.defer=1;
	}
	var x = document.getElementsByTagName('head')[0];
	x.appendChild(js);
	return 0;
}
function insertCSS(CSSURL){
	var css = document.createElement('link');
	css.href = CSSURL;
	css.rel = 'stylesheet';
	document.getElementsByTagName('head')[0].appendChild(css);
	return 0;
}
function loadScripts(){
//	insertJSLib('leaflet.TileLayer.WMTS/leaflet-tilelayer-wmts.js');
//	insertJSLib('leaflet-control-geocoder/Control.Geocoder.js');
//	insertCSS('leaflet-control-geocoder/Control.Geocoder.css');
//	insertJSLib('Leaflet.GridLayer.GoogleMutant/Leaflet.GoogleMutant.js');
//	insertJSLib('leaflet.label.js');
//	insertJSLib('md5.min.js');
//	insertJSLib('map.js');
	return 0;
}
function copyToClipboard(input) {
	window.prompt("Copy to clipboard: Ctrl+C, Enter", input);
}
// http://stackoverflow.com/questions/18082/validate-decimal-numbers-in-javascript-isnumeric#1830844
function isNumeric(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}
// http://stackoverflow.com/questions/2705583/how-to-simulate-a-click-with-javascript#2706236
/**
 * Trigger the specified event on the specified element.
 * @param  {Object} elem  the target element.
 * @param  {String} event the type of the event (e.g. 'click').
 */
function triggerEvent( elem, event ) {
	var clickEvent = new Event( event ); // Create the event.
	elem.dispatchEvent( clickEvent );    // Dispatch the event.
}
